package com.wwwgomes.examples.service;

import com.wwwgomes.examples.food.api.di.modelo.Cliente;
import com.wwwgomes.examples.modelo.Produto;
import com.wwwgomes.examples.notificador.Notificador;

public class EmissaoNotaFiscalService {

	private Notificador notificador;
	
	public EmissaoNotaFiscalService(Notificador notificador) {
		this.notificador = notificador;
	}
	
	public void emitir(Cliente cliente, Produto produto) {
		
		this.notificador.notificar(cliente, "Nota fiscal do produto "
				+ produto.getNome() + " foi emitida!");
	}
	
}
