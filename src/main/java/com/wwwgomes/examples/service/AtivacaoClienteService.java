package com.wwwgomes.examples.service;

import com.wwwgomes.examples.food.api.di.modelo.Cliente;
import com.wwwgomes.examples.notificador.Notificador;

public class AtivacaoClienteService {

private Notificador notificador;
	
	public AtivacaoClienteService(Notificador notificador) {
		this.notificador = notificador;
	}
	
	public void ativar(Cliente cliente) {
		cliente.ativar();
		
		this.notificador.notificar(cliente, "Seu cadastro no sistema está ativo!");
	}
	
}
