package com.wwwgomes.examples;

import java.util.ArrayList;

import com.wwwgomes.examples.food.api.di.modelo.Cliente;
import com.wwwgomes.examples.notificador.Notificador;
import com.wwwgomes.examples.notificador.NotificadorEmail;
import com.wwwgomes.examples.notificador.NotificadorSMS;
import com.wwwgomes.examples.service.AtivacaoClienteService;

public class Main {

	public static void main(String[] args) {
		
		var joao = new Cliente("João", "joao@xyz.com", "3499998888");
		var maria = new Cliente("Maria", "maria@xyz.com", "1177772222");
		
		var notificadores = new ArrayList<Notificador>();
		var email = new NotificadorEmail();
		var sms = new NotificadorSMS();
		
		notificadores.add(email);
		notificadores.add(sms);
		
		for (Notificador notificador : notificadores) {
			var ativacaoCliente = new AtivacaoClienteService(notificador);
			
			ativacaoCliente.ativar(joao);
			ativacaoCliente.ativar(maria);
		}
		
	}
	
}
