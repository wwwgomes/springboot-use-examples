package com.wwwgomes.examples.modelo;

import java.math.BigDecimal;

import lombok.Getter;

public class Produto {

	@Getter	private String nome;
	@Getter	private BigDecimal valorTotal;

	public Produto(String nome, BigDecimal valorTotal) {
		this.nome = nome;
		this.valorTotal = valorTotal;
	}

}
