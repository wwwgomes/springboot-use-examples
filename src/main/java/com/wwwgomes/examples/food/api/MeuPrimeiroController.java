package com.wwwgomes.examples.food.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wwwgomes.examples.food.api.di.modelo.Cliente;
import com.wwwgomes.examples.food.api.di.service.AtivacaoClienteService;

@Controller
public class MeuPrimeiroController {
	
	private AtivacaoClienteService ativacaoClienteService;
	
	public MeuPrimeiroController(AtivacaoClienteService ativacaoClienteService) {
		this.ativacaoClienteService = ativacaoClienteService;
		
		System.out.println("MeuPrimeiroController: " + ativacaoClienteService);
	}
	
	@GetMapping("/hello")
	@ResponseBody
	public String hello() {
		var joao = new Cliente("João", "joao@xyz.com", "3499998888");
		
		ativacaoClienteService.ativar(joao);
		
		return "Hello!";
	}

}
