package com.wwwgomes.examples.food.api.di.notificador;

import org.springframework.stereotype.Component;

import com.wwwgomes.examples.food.api.di.modelo.Cliente;

@TipoNotificador(NivelUrgencia.URGENTE)
@Component
public class NotificadorSMS implements Notificador {

	@Override
	public void notificar(Cliente cliente, String mensagem) {
		System.out.printf("Notificando %s por SMS através do telefone %s: %s\n", 
				cliente.getNome(), cliente.getTelefone(), mensagem);
	}

}
