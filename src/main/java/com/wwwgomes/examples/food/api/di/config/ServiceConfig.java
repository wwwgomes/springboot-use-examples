package com.wwwgomes.examples.food.api.di.config;

import org.springframework.context.annotation.Bean;

import com.wwwgomes.examples.food.api.di.service.AtivacaoClienteService;

//@Configuration
public class ServiceConfig {

	@Bean(initMethod = "init", destroyMethod = "destroy")
	public AtivacaoClienteService ativacaoClienteService() {
		return new AtivacaoClienteService();
	}
	
}
