package com.wwwgomes.examples.food.api.di.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.wwwgomes.examples.food.api.di.notificador.NivelUrgencia;
import com.wwwgomes.examples.food.api.di.notificador.Notificador;
import com.wwwgomes.examples.food.api.di.notificador.TipoNotificador;
import com.wwwgomes.examples.food.api.di.service.ClienteAtivadoEvent;

@Component
public class NotificacaoService {
	
	private Notificador notificador;

	@TipoNotificador(NivelUrgencia.SEM_URGENCIA)
	@Autowired
	public NotificacaoService(Notificador notificador) {
		this.notificador = notificador;
	}
	
	@EventListener
	public void clienteAtivadoEvent(ClienteAtivadoEvent event) {
		notificador.notificar(event.getCliente(), "Seu cadastro no sistema está ativo!");
	}

}
