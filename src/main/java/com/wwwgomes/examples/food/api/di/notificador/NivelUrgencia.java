package com.wwwgomes.examples.food.api.di.notificador;

public enum NivelUrgencia {
	URGENTE, SEM_URGENCIA;
}
