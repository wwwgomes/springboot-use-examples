package com.wwwgomes.examples.food.api.di.notificador;

import com.wwwgomes.examples.food.api.di.modelo.Cliente;

public interface Notificador {

	void notificar(Cliente cliente, String mensagem);
	
}
