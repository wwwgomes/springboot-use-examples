package com.wwwgomes.examples.food.api.di.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import com.wwwgomes.examples.food.api.di.modelo.Cliente;

@Component
public class AtivacaoClienteService {

	private ApplicationEventPublisher applicationEventPublisher;
	
	@Autowired
	public AtivacaoClienteService(ApplicationEventPublisher applicationEventPublisher) {
		this.applicationEventPublisher = applicationEventPublisher;
	}
	
	public AtivacaoClienteService() {
	}
	
	public void ativar(Cliente cliente) {
		cliente.ativar();

		applicationEventPublisher.publishEvent(new ClienteAtivadoEvent(cliente));
	}
	
}
