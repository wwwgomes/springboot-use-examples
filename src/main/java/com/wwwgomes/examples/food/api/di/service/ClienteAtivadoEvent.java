package com.wwwgomes.examples.food.api.di.service;

import com.wwwgomes.examples.food.api.di.modelo.Cliente;

import lombok.Getter;

public class ClienteAtivadoEvent {
	
	@Getter private Cliente cliente;

	public ClienteAtivadoEvent(Cliente cliente) {
		this.cliente = cliente;
	}
	
}
